# SPDX-FileCopyrightText: 2022 debgerme <fossgerme@tuta.io>
#
# SPDX-License-Identifier: GPL-3.0-or-later

ln -s application-x-executable-symbolic.symbolic.png application-x-sharedlib-symbolic.symbolic.png
