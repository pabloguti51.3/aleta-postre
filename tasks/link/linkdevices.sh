# SPDX-FileCopyrightText: 2022 debgerme <fossgerme@tuta.io>
#
# SPDX-License-Identifier: GPL-3.0-or-later

ln -s drive-harddisk-symbolic.symbolic.png drive-harddisk-usb-symbolic.symbolic.png 
ln -s drive-harddisk-symbolic.symbolic.png drive-harddisk-system-symbolic.symbolic.png 
ln -s media-optical-symbolic.symbolic.png media-optical-dvd-symbolic.symbolic.png
ln -s media-optical-symbolic.symbolic.png media-optical-bd-symbolic.symbolic.png
ln -s media-optical-symbolic.symbolic.png media-optical-cd-symbolic.symbolic.png
ln -s media-optical-symbolic.symbolic.png media-optical-cd-audio-symbolic.symbolic.png
