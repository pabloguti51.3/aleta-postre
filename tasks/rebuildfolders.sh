#!/bin/bash

# SPDX-FileCopyrightText: 2022 debgerme <fossgerme@tuta.io>
#
# SPDX-License-Identifier: GPL-3.0-or-later

# this creates the To export folders. the .svg will be placed here
mkdir -p _build/icons-t/apps
mkdir -p _build/icons-t/places
mkdir -p _build/icons-t/categories
mkdir -p _build/icons-t/devices
mkdir -p _build/icons-t/status
mkdir -p _build/icons-t/mimetypes
mkdir -p _build/icons-t/actions
mkdir -p _build/icons-t/animations
mkdir -p _build/icons-t/emblems
mkdir -p _build/icons-t/ui

mkdir -p _build/icons-t/apps-symbolic
mkdir -p _build/icons-t/places-symbolic
mkdir -p _build/icons-t/categories-symbolic
mkdir -p _build/icons-t/devices-symbolic
mkdir -p _build/icons-t/status-symbolic
mkdir -p _build/icons-t/mimetypes-symbolic
mkdir -p _build/icons-t/actions-symbolic
mkdir -p _build/icons-t/animations-symbolic
mkdir -p _build/icons-t/emblems-symbolic
mkdir -p _build/icons-t/ui-symbolic

# this creates the Exported folders. the exported svg will be placed here
mkdir -p _build/icons-e/apps
mkdir -p _build/icons-e/places
mkdir -p _build/icons-e/categories
mkdir -p _build/icons-e/devices
mkdir -p _build/icons-e/status
mkdir -p _build/icons-e/mimetypes
mkdir -p _build/icons-e/actions
mkdir -p _build/icons-e/animations
mkdir -p _build/icons-e/emblems
mkdir -p _build/icons-e/ui

mkdir -p _build/icons-e/apps-symbolic
mkdir -p _build/icons-e/places-symbolic
mkdir -p _build/icons-e/categories-symbolic
mkdir -p _build/icons-e/devices-symbolic
mkdir -p _build/icons-e/status-symbolic
mkdir -p _build/icons-e/mimetypes-symbolic
mkdir -p _build/icons-e/actions-symbolic
mkdir -p _build/icons-e/animations-symbolic
mkdir -p _build/icons-e/emblems-symbolic
mkdir -p _build/icons-e/ui-symbolic

# this creates the build folders. here is the result of the build. here will be the index.theme and the PNG exported from the svg
mkdir -p _build/aleta/apps
mkdir -p _build/aleta/places
mkdir -p _build/aleta/categories
mkdir -p _build/aleta/devices
mkdir -p _build/aleta/status
mkdir -p _build/aleta/mimetypes
mkdir -p _build/aleta/actions
mkdir -p _build/aleta/animations
mkdir -p _build/aleta/emblems

# here is the .png exported.
mkdir -p _build/aleta/apps/8
mkdir -p _build/aleta/apps/16
mkdir -p _build/aleta/apps/24
mkdir -p _build/aleta/apps/32
mkdir -p _build/aleta/apps/48
mkdir -p _build/aleta/apps/64
mkdir -p _build/aleta/apps/96
mkdir -p _build/aleta/apps/128
mkdir -p _build/aleta/apps/256
mkdir -p _build/aleta/apps/scalable

mkdir -p _build/aleta/ui/16
mkdir -p _build/aleta/ui/24
mkdir -p _build/aleta/ui/32
mkdir -p _build/aleta/ui/48
mkdir -p _build/aleta/ui/64
mkdir -p _build/aleta/ui/scalable

mkdir -p _build/aleta/places/8
mkdir -p _build/aleta/places/16
mkdir -p _build/aleta/places/24
mkdir -p _build/aleta/places/32
mkdir -p _build/aleta/places/48
mkdir -p _build/aleta/places/64
mkdir -p _build/aleta/places/96
mkdir -p _build/aleta/places/128
mkdir -p _build/aleta/places/256
mkdir -p _build/aleta/places/scalable

mkdir -p _build/aleta/categories/8
mkdir -p _build/aleta/categories/16
mkdir -p _build/aleta/categories/22
mkdir -p _build/aleta/categories/24
mkdir -p _build/aleta/categories/32
mkdir -p _build/aleta/categories/48
mkdir -p _build/aleta/categories/64
mkdir -p _build/aleta/categories/96
mkdir -p _build/aleta/categories/128
mkdir -p _build/aleta/categories/scalable

mkdir -p _build/aleta/devices/16
mkdir -p _build/aleta/devices/24
mkdir -p _build/aleta/devices/32
mkdir -p _build/aleta/devices/48
mkdir -p _build/aleta/devices/64
mkdir -p _build/aleta/devices/96
mkdir -p _build/aleta/devices/scalable

mkdir -p _build/aleta/status/8
mkdir -p _build/aleta/status/16
mkdir -p _build/aleta/status/24
mkdir -p _build/aleta/status/32
mkdir -p _build/aleta/status/48
mkdir -p _build/aleta/status/64
mkdir -p _build/aleta/status/96
mkdir -p _build/aleta/status/128
mkdir -p _build/aleta/status/256
mkdir -p _build/aleta/status/scalable

mkdir -p _build/aleta/mimetypes/8
mkdir -p _build/aleta/mimetypes/16
mkdir -p _build/aleta/mimetypes/24
mkdir -p _build/aleta/mimetypes/32
mkdir -p _build/aleta/mimetypes/48
mkdir -p _build/aleta/mimetypes/64
mkdir -p _build/aleta/mimetypes/96
mkdir -p _build/aleta/mimetypes/128
mkdir -p _build/aleta/mimetypes/256
mkdir -p _build/aleta/mimetypes/scalable

mkdir -p _build/aleta/actions/8
mkdir -p _build/aleta/actions/16
mkdir -p _build/aleta/actions/22
mkdir -p _build/aleta/actions/24
mkdir -p _build/aleta/actions/32
mkdir -p _build/aleta/actions/48
mkdir -p _build/aleta/actions/64
mkdir -p _build/aleta/actions/96
mkdir -p _build/aleta/actions/128
mkdir -p _build/aleta/actions/256
mkdir -p _build/aleta/actions/scalable

mkdir -p _build/aleta/animations/8
mkdir -p _build/aleta/animations/16
mkdir -p _build/aleta/animations/24
mkdir -p _build/aleta/animations/32
mkdir -p _build/aleta/animations/48
mkdir -p _build/aleta/animations/64
mkdir -p _build/aleta/animations/96
mkdir -p _build/aleta/animations/128
mkdir -p _build/aleta/animations/scalable

mkdir -p _build/aleta/emblems/16
mkdir -p _build/aleta/emblems/24
mkdir -p _build/aleta/emblems/32
mkdir -p _build/aleta/emblems/48
mkdir -p _build/aleta/emblems/64
mkdir -p _build/aleta/emblems/scalable
