#!/bin/bash

# SPDX-FileCopyrightText: 2022 debgerme <fossgerme@tuta.io>
#
# SPDX-License-Identifier: GPL-3.0-or-later

tasks/export/actions.sh
tasks/export/animations.sh
tasks/export/apps.sh
tasks/export/categories.sh
tasks/export/devices.sh
tasks/export/emblems.sh
tasks/export/mimetypes.sh
tasks/export/places.sh
tasks/export/status.sh
tasks/export/ui.sh

tasks/export-symbolic/actions-sym.sh
tasks/export-symbolic/animations-sym.sh
tasks/export-symbolic/apps-sym.sh
tasks/export-symbolic/categories-sym.sh
tasks/export-symbolic/devices-sym.sh
tasks/export-symbolic/emblems-sym.sh
tasks/export-symbolic/mimetypes-sym.sh
tasks/export-symbolic/places-sym.sh
tasks/export-symbolic/status-sym.sh
tasks/export-symbolic/ui-sym.sh
