<!--
SPDX-FileCopyrightText: 2022 debgerme <fossgerme@tuta.io>

SPDX-License-Identifier: Unlicense
-->

user-available-symbolic
airplane-mode-symbolic
auth-fingerprint-symbolic
x-office-calendar-symbolic
