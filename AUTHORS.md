<!--
SPDX-FileCopyrightText: 2022 debgerme <fossgerme@tuta.io>

SPDX-License-Identifier: CC-BY-SA-4.0
-->

## List of contributors

* [germedeb](https://gitea.sergiotarxz.freemyip.com/germedeb) - creator of the icon pack
* [Simon](https://gitea.sergiotarxz.freemyip.com/Simon) - making icons
* [sergiotarxz](https://gitea.sergiotarxz.freemyip.com/sergiotarxz) - coding

## Forked from

#### Base Project

* [Ameixa](https://gitlab.com/xphnx/ameixa) Minimalist material design inspired icon pack for Android

#### How the icon pack is structured:

* Adwaita icon theme

## Inspiration

* Adwaita icon theme
* Mint-y icon theme
* symbolic icons from Adwaita icon theme
* Moka icon theme
* Android adaptive icons

## Special thanks to

The Community that helped commenting or something
The GNOME proyect. Some symbolic icons are modifications or copies of Adwaita's icons
